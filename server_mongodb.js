// Basic Configuration 
const bodyParser = require('body-parser');
const chalk = require('chalk');
const cors = require('cors');
const dns = require('dns');
const express = require('express');
const mongoose = require('mongoose');
const port = process.env.PORT || 3000;
const pug = require('pug');
const request = require('request');
require('dotenv').config();

const app = express();
const autoIncrement = require('mongoose-sequence')(mongoose);

app.set('view engine', 'pug')
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true});
const connection = mongoose.connection;
connection.on('open', function (ref) {
        console.log('Connected to mongo server. ');       
});

const linkSchema = new mongoose.Schema({
  id: Number,
  link: { type: String, unique: true },
  shortLink: String
});

linkSchema.path('link').index({ unique: true });

linkSchema.plugin(autoIncrement, {inc_field: 'id'});    // enable autoincrement

const Link = mongoose.model('Link', linkSchema);

function listCollections(){    // allows to print all collections in db
const connection = mongoose.connection;
connection.on('open', function (ref) {
        console.log('Connected to mongo server. ');
        connection.db.listCollections().toArray(function(err, names) {
        if (err) {
        console.log(err);
        }
        else {
        names.forEach(function(collection) {
            console.log("--->", collection.name);
        });
        }});        
});
}

function to62(num){   // convert integer to base 62
  console.log("Entered to62");
  var digits = [];
  var remainder;
  num = parseInt(num);
  while (num > 0){
  remainder = Math.round(num % 62);
  digits.push(remainder);
  num = Math.round(num / 62);
  }
    return digits.reverse();
}

function toShortLink(nums){     // make a new link from an array
  console.log("Entered toShortLink");
  var symbols = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  var shortenedLink = "";
  for (var i = 0; i < nums.length; i++){
    shortenedLink += symbols[nums[i]];
  }
  return shortenedLink;
}

function parseLink(link){
  let address = link;
  let reg = /(http(s)?:\/\/)?(www.)?(\w+)+(.)(\w+)+(\/)?/;
  let matched = address.match(reg);
  let linkBody = '';
  for (let i = 4; i <= 6; i++){     // select site.domain part
      linkBody += matched[i];
  }
    return linkBody;
}

function parseHTTPProtocol(link){
  let address = link;
  let regHTTPWWW = /(http(s)?:\/\/)?(www.)?/;
  let regHTTP = /(http(s)?:\/\/)?/;
  let regWWW = /(www.)?/;
  if (address.match(regHTTPWWW)[0] === 'https://www.' || address.match(regHTTPWWW)[0] === 'http://www.' || address.match(regHTTPWWW)[0] === 'http://' || address.match(regHTTPWWW)[0] === 'https://'){
    return link;
  }
  else if (address.match(regWWW)[0] === "www."){
  return 'http://' + link;
  }
   else{
  return 'http://' + link;
  }
}

app.get('/', function(req, res){
  res.sendFile(process.cwd() + '/views/index.html');
});

app.post('/api/shorturl/new', postParse);

function postParse(req, res){
  let address = req.body.url;
  if (address === ''){
    res.render('error', { messageError: 'You entered an empty URL! Try again. ', target: 'http://' + req.get('host')})
  }
  else{
  let linkBody = parseLink(address);
  let result = dns.lookup(linkBody, function (err, addresses, family) {
    if (addresses == undefined){
      res.render('error', { messageError: 'You entered an invalid URL! Try again. ', target: 'http://' + req.get('host')})
    }
    else {
      Link.find({link: address}, function(error, data){
        if (error) {
          res.render('error', { messageError: 'Canot use the database now. Try again later ', target: 'http://' + req.get('host')})}
        else {
          if (data.length == 0){
          Link.create({ link: address }, function (error, address) {
            if (error) {
              res.render('error', { messageError: 'Cannot insert your link. Try again later. ', target: 'http://' + req.get('host')})
            }
            else {
              Link.find({link: address.link}, function(error, data){
                if (error) {
                  res.render('error', { messageError: 'Cannot find your link. Try again. ', target: 'http://' + req.get('host')});}
                else {
                  var convertTo62Base = toShortLink(to62(data[0].id));
                  data[0].shortLink = req.get('host') + "/api/shorturl/" + convertTo62Base;
                  data[0].save(function (error, data) {
                      if (err) { 
                        res.render('error', { messageError: 'Cannot save your short link. ', target:  req.get('host')})
                       }
                      else {
                       res.render('success', { messageSuccess: 'Short link was created. You can visit it here:', target: 'http://' + data.shortLink, shortLink: data.shortLink })
                     }
                    });   
                 }
              });
            }
          });
         }
         else if (data.length == 1){
          res.render('success', { messageSuccess: 'Short link had already been created. You can visit it here:', target: 'http://' + data[0].shortLink, shortLink: data[0].shortLink })
         }
        }});
       }
  });
}
}

app.get('/api/shorturl/:code', handler);

function handler(req, res){
  let link = req.get('host') + "/api/shorturl/" + req.params.code;
  Link.find({shortLink: link}, function(error, data){
        if (error) {
          res.render('error', { messageError: 'Cannot find your link. ', target:  'http://' + req.get('host')})
        }
        else {
          let protocol = parseHTTPProtocol(data[0].link);
          res.writeHead(301,
          {Location: protocol}
          );
          res.end();
        }
  });
}
  
// your first API endpoint... 
app.get("/api/hello", function (req, res) {
  res.json({greeting: 'hello API'});
});

app.listen(port, function () {
  console.log(chalk.red('Node.js listening on port', port + "\r\n"));
});