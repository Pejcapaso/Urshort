// Basic Configuration 
const bodyParser = require('body-parser');
const chalk = require('chalk');
const cors = require('cors');
const dns = require('dns');
const express = require('express');
const { Pool, Client } = require('pg');  // psql
const connectionString = 'postgresql://babyngzieendoh:d2c2dba96174090725613b93d0e1b58f72d9197e6048871d7c22e9a28cf893ee@ec2-54-247-170-5.eu-west-1.compute.amazonaws.com:5432/d721ibcn2nqei6?ssl=true';
const port = process.env.PORT || 3000;
const pug = require('pug');
const request = require('request');
require('dotenv').config();

const app = express();

app.set('view engine', 'pug')
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));

// psql
const pool = new Pool({
    connectionString: connectionString,
})


function to62(num){   // convert integer to base 62
    var digits = [];
    var remainder;
    num = parseInt(num);
    while (num > 0){
        remainder = Math.round(num % 62);
        digits.push(remainder);
        num = Math.round(num / 62);
    }
    //console.log("result of to62: ", digits.reverse());
    return digits.reverse();
}

function toShortLink(nums){     // make a new link from an array
    var symbols = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                  '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var shortenedLink = "";
    for (var i = 0; i < nums.length; i++){
        shortenedLink += symbols[nums[i]];
    }
    //console.log("result of toShortLink: ", shortenedLink);
    return shortenedLink;
}

function parseLink(link){
    let address = link;
    let reg = /(http(s)?:\/\/)?(www.)?(\w+)+(.)(\w+)+(\/)?/;
    let matched = address.match(reg);
    let linkBody = '';
    for (let i = 4; i <= 6; i++){     // select site.domain part
        linkBody += matched[i];
    }
    //console.log("result of parseLink: ", linkBody);
    return linkBody;
}

function parseHTTPProtocol(link){
    let address = link;
    let regHTTPWWW = /(http(s)?:\/\/)?(www.)?/;
    let regHTTP = /(http(s)?:\/\/)?/;
    let regWWW = /(www.)?/;
    if (address.match(regHTTPWWW)[0] === 'https://www.' || address.match(regHTTPWWW)[0] === 'http://www.' || address.match(regHTTPWWW)[0] === 'http://' || address.match(regHTTPWWW)[0] === 'https://'){    
        return link;
    }
    else if (address.match(regWWW)[0] === "www."){
        return 'http://' + link;
    }
    else{
        return 'http://' + link;
    }
}

app.get('/', function(req, res) {
    res.sendFile(process.cwd() + '/views/index.html');
});

app.post('/api/shorturl/new', postParse);

function postParse(request, response) {
    let address = request.body.url;
    if (address === ''){
        response.render('error', { messageError: 'You entered an empty URL! Try again. ', target: 'http://' + request.get('host')})
    }
    else {
        let linkBody = parseLink(address);
        let result = dns.lookup(linkBody, function (err, addresses, family) {
            if (addresses == undefined){
                res.render('error', { messageError: 'You entered an invalid URL! Try again. ', target: 'http://' + request.get('host')})
            }
            else {
                //console.log("Got your link: ", linkBody);
                let foundResults = [];
                pool.query('SELECT * FROM links WHERE link = $1;', [linkBody], (error, result) => {
   	                if (error) {
   		                throw error;
                        response.render('error', { messageError: error});
   		            }
   		            else {
                        //console.log("No error in first SELECT, keep going...", result.rows);
   			            if (result.rows.length == 0){
   			  	            //console.log("Didn't find your link in the db, trying to create...");
   		 	                pool.query('INSERT INTO links (link, shortlink) VALUES ($1, $2)', [linkBody, linkBody], error => {
   		 		                if (error) {
   						            console.log("Can't insert your link");
                                    response.render('error', { messageError: error});
   		 				            throw error;
   		 			            }
   		 			            else {
   						            //console.log("Inserting your link...");
   				 	                pool.query('SELECT * FROM links WHERE link = $1', [linkBody], (error, result) => {
   				 		                if (error) {
   								            //console.log("Can't retrieve your link");
   				 				            throw error;
   				 			            }
   				 			            else {
                                            let convertTo62Base = toShortLink(to62(result.rows[0].id));
                                            let shortLink = request.get('host') + "/api/shorturl/" + convertTo62Base;
                                            //console.log("Short link: ", shortLink);
                                            pool.query('UPDATE links SET shortlink = $1 WHERE id = $2;', [shortLink, result.rows[0].id], error => {
                                                if (error) {
                                                throw error
                                                }
                                                else {
                                                    response.render('success', { messageSuccess: 'Short link was created. You can visit it here:', target: 'http://' + shortLink, shortLink: shortLink});
                                                }
                                            });
   							            }
   					 	            });
   					            }
   			 	            });    
   			            }
   	     	
                        else if (result.rows.length != 0){
                            //console.log("Link has been in the db already");
                            response.render('success', { messageSuccess: 'Short link had already been created. You can visit it here:', target: 'http://' + result.rows[0].shortlink, shortLink: result.rows[0].shortlink});
                        }
   		            }
   	            });
            }
        });
    }
}		   
		   

app.get('/api/shorturl/:code', handler);

function handler(req, res){
    let link = req.get('host') + "/api/shorturl/" + req.params.code;
    pool.query('SELECT * FROM links WHERE shortlink = $1;', [link], (error, result) => {
       if (error) {
           res.render('error', { messageError: 'Cannot find your link. ', target:  'http://' + req.get('host')});
           throw error;
       }
       else {
           //console.log("result: ", result);
           let protocol = parseHTTPProtocol(result.rows[0].link);
           res.writeHead(301, {Location: protocol});
           res.end();
       }
    });
}
  
// your first API endpoint... 
app.get("/api/hello", function (req, res) {
    res.json({greeting: 'hello API'});
});

app.listen(port, function () {
    console.log(chalk.red('Node.js listening on port', port + "\r\n"));
});